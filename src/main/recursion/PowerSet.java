package recursion;

import java.util.*;

/**
 * Created by Виктор on 08.11.2016.
 */
public class PowerSet {

    private final List<Set<Integer>> subsets;

    public PowerSet(Set<Integer> set) {
//        subsets = generateIterativeSubsets(set);
        subsets = generateSubsets(set);
    }

    public static void main(String[] args) {
        Set<Integer> set = new TreeSet<>();
        set.add(1);
        set.add(2);
        set.add(3);
        set.add(4);
        PowerSet powerSet = new PowerSet(set);
        List<Set<Integer>> subsets = powerSet.getAllSubsets();
        System.out.println(Arrays.deepToString(subsets.toArray()));
    }

    private List<Set<Integer>> generateSubsets(Set<Integer> set) {
        if (set.isEmpty()) {
            ArrayList<Set<Integer>> subsets = new ArrayList<>();
            subsets.add(new HashSet<>());
            return subsets;
        }
        Iterator<Integer> it = set.iterator();
        Integer elem = it.next();
        it.remove();
        List<Set<Integer>> subsets = generateSubsets(set);
        ListIterator<Set<Integer>> listIt = subsets.listIterator();
        while (listIt.hasNext()) {
            Set<Integer> subset = listIt.next();
            Set<Integer> newSubset = new HashSet<>(subset);
            newSubset.add(elem);
            listIt.add(newSubset);
        }
        return subsets;
    }

    private List<Set<Integer>> generateIterativeSubsets(Set<Integer> set) {
        int num = pow2(set.size());
        List<Set<Integer>> subsets = new LinkedList<>();
        for (int i = 0; i < num; i++) {
            subsets.add(createSubset(set, i));
        }
        return subsets;
    }

    private Set<Integer> createSubset(Set<Integer> set, int number) {
        Set<Integer> subset = new TreeSet<>();
        Integer[] setArray = set.toArray(new Integer[]{});
        for (int i = 0; i < setArray.length; i++) {
            if (isBit(number, i)) subset.add(setArray[i]);
        }
        return subset;
    }

    boolean isBit(int value, int index) {
        if (((value >> index) & 1) > 0) return true;
        return false;
    }

    int pow2(int size) {
        if (size < 0) throw new IllegalArgumentException();
        else if (size == 0) return 1;
        int result = 2;
        for (int i = 2; i <= size; i++) {
            result *= 2;
        }
        return result;
    }

    public List<Set<Integer>> getAllSubsets() {
        return subsets;
    }
}
