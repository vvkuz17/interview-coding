package recursion;

/**
 * Created by Виктор on 08.11.2016.
 */
public class RecursiveMultiply {

    public static void main(String[] args) {
        System.out.println(multiply(0, -10));
    }

    /**
     * Not effective, may be O(log(min(a,b))
     *
     * @param a
     * @param b
     * @return
     */
    private static long multiply(int a, int b) {
        if (a == 0 || b == 0) return 0;
        if (Math.abs(b) > Math.abs(a)) {
            int x = a;
            a = b;
            b = x;
        }
        if (b == 1) return a;
        if (b == -1) return -a;
        return execMultiply(a, b);
    }

    private static long execMultiply(int a, int b) {
        if ((a > 0 & b > 0))
            return execMultiply(a, b - 1) + a;
        else if (a < 0 & b < 0)
            return execMultiply(-a, (-b) - 1) - a;
        else if (a < 0 & b > 0)
            return -execMultiply(-a, b - 1) - a;
        else if (a > 0 & b < 0)
            return -(execMultiply(a, (-b) - 1) + a);
        return 0;
    }

}
