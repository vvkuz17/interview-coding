package recursion;

import java.util.Arrays;

/**
 * Created by Виктор on 09.11.2016.
 */
public class EightQueens {

    public static final int GRID_SIZE = 8;

    public static void main(String[] args) {
        int[] positions = new int[GRID_SIZE];
        int row = GRID_SIZE - 1;
        placeQueens(positions, row);
    }

    private static void placeQueens(int[] positions, int row) {
        if (row < 0)
            System.out.println(Arrays.toString(positions));
        else {
            for (int col = 0; col < GRID_SIZE; col++) {
                if (checkValid(positions, row, col)) {
                    positions[row] = col;
                    placeQueens(positions, row - 1);
                }
            }
        }
    }

    private static boolean checkValid(int[] positions, int row, int col) {
        for (int i = GRID_SIZE - 1; i > row; i--) {
            if (col == positions[i])
                return false;
            if ((i - row) == Math.abs(col - positions[i]))
                return false;
        }
        return true;
    }

}
