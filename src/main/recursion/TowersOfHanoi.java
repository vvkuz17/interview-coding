package recursion;

import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;

/**
 * Created by Виктор on 08.11.2016.
 */
public class TowersOfHanoi {

    private final Deque<Disk> tower1 = new LinkedList<>();
    private final Deque<Disk> tower2 = new LinkedList<>();
    private final Deque<Disk> tower3 = new LinkedList<>();

    public TowersOfHanoi(int num) {
        for (int i = num; i >= 1; i--) {
            tower1.add(new Disk(i));
        }
    }

    public static void main(String[] args) {
        int num = 5;
        TowersOfHanoi towersOfHanoi = new TowersOfHanoi(num);
        System.out.println("Before: ");
        System.out.println(Arrays.deepToString(towersOfHanoi.getTower1().toArray()));
        System.out.println(Arrays.deepToString(towersOfHanoi.getTower3().toArray()));

        towersOfHanoi.moveDisks(num);

        System.out.println("After: ");
        System.out.println(Arrays.deepToString(towersOfHanoi.getTower1().toArray()));
        System.out.println(Arrays.deepToString(towersOfHanoi.getTower3().toArray()));
    }

    public void moveDisks(int num) {
        move(num, tower1, tower3);
    }

    public void move(int num, Deque<Disk> fromTower, Deque<Disk> toTower) {
        if (num == 1) {
            toTower.push(fromTower.pop());
        } else {
            Deque<Disk> otherTower = getOtherTower(fromTower, toTower);
            move(num - 1, fromTower, otherTower);
            move(1, fromTower, toTower);
            move(num - 1, otherTower, toTower);
        }
    }

    private Deque<Disk> getOtherTower(Deque<Disk> fromTower, Deque<Disk> toTower) {
        if ((fromTower == tower2 & toTower == tower1) | (fromTower == tower1 & toTower == tower2)) return tower3;
        else if ((fromTower == tower2 & toTower == tower3) | (fromTower == tower3 & toTower == tower2)) return tower1;
        else if ((fromTower == tower1 & toTower == tower3) | (fromTower == tower3 & toTower == tower1)) return tower2;
        return null;
    }

    public Deque<Disk> getTower1() {
        return tower1;
    }

    public Deque<Disk> getTower2() {
        return tower2;
    }

    public Deque<Disk> getTower3() {
        return tower3;
    }

    static class Disk {

        private final int size;

        Disk(int size) {
            this.size = size;
        }

        int getSize() {
            return size;
        }

        @Override
        public String toString() {
            return String.valueOf(size);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Disk disk = (Disk) o;

            return size == disk.size;

        }

        @Override
        public int hashCode() {
            return size;
        }
    }

}
