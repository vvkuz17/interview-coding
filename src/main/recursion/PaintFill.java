package recursion;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Queue;

/**
 * Created by Виктор on 08.11.2016.
 */
public class PaintFill {

    public static void main(String[] args) {
        Image image = new Image(new int[][]{
                {1, 1, 1, 1},
                {1, 2, 2, 1},
                {1, 2, 2, 1},
                {1, 1, 1, 1}
        });
        image.paintFill(0, 0, 3);
        System.out.println(image.toString());
    }

    static class Point {

        final int row;
        final int col;

        Point(int row, int col) {
            this.row = row;
            this.col = col;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Point point = (Point) o;

            if (row != point.row) return false;
            return col == point.col;

        }

        @Override
        public int hashCode() {
            int result = row;
            result = 31 * result + col;
            return result;
        }
    }

    static class Image {

        private final int[][] image;

        public Image(int[][] image) {
            if (image == null || image.length == 0)
                throw new IllegalArgumentException("Image must be non null and non empty.");
            this.image = image;
        }

        public void paintFill(int row, int col, int newValue) {
            int oldValue = image[row][col];
            Queue<Point> queue = new ArrayDeque<>();
            boolean[][] visited = new boolean[image.length][image[0].length];
            for (int i = 0; i < visited.length; i++) {
                Arrays.fill(visited[i], false);
            }
            queue.add(new Point(row, col));

            while (!queue.isEmpty()) {
                Point point = queue.poll();
                int r = point.row;
                int c = point.col;
                if (image[r][c] != oldValue) continue;
                image[r][c] = newValue;
                visited[r][c] = true;
                addPoint(r - 1, c, visited, queue);
                addPoint(r + 1, c, visited, queue);
                addPoint(r, c - 1, visited, queue);
                addPoint(r, c + 1, visited, queue);
            }
        }

        private void addPoint(int row, int col, boolean[][] visited, Queue<Point> queue) {
            if (row < 0 || row > image.length - 1) return;
            if (col < 0 || col > image[0].length - 1) return;
            if (!visited[row][col]) queue.add(new Point(row, col));
        }

        @Override
        public String toString() {
            return Arrays.deepToString(image);
        }
    }

}
