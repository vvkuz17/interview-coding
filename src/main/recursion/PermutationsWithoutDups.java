package recursion;

import java.util.*;

/**
 * Created by Виктор on 08.11.2016.
 */
public class PermutationsWithoutDups {

    public static void main(String[] args) {
        System.out.println(Arrays.deepToString(generate("accccccccc").toArray()));
    }

    private static List<String> permutations(String string) {
        if (string == null || string.isEmpty()) return Collections.emptyList();
        char[] charsArray = string.toCharArray();
        List<Character> charsList = new ArrayList<>();
        for (char c : charsArray) {
            charsList.add(c);
        }
        List<List<Character>> perms = generate(charsList);

        List<String> result = new ArrayList<>(perms.size());
        for (List<Character> perm : perms) {
            StringBuilder sb = new StringBuilder();
            for (Character c : perm) {
                sb.append(c);
            }
            result.add(sb.toString());
        }
        return result;
    }

    private static Set<String> generate(String string) {
        Set<String> perms = new HashSet<>();
        if (string.length() == 1) {
            perms.add(string);
        } else {
            for (int i = 0; i < string.length(); i++) {
                char c = string.charAt(i);
                String substring = new StringBuilder(string).deleteCharAt(i).toString();
                Set<String> prevPerms = generate(substring);
                for (String perm : prevPerms) {
                    perms.add(c + perm);
                }
            }
        }
        return perms;
    }

    private static List<List<Character>> generate(List<Character> chars) {
        List<List<Character>> perms = new ArrayList<>();
        if (chars.size() == 1) {
            ArrayList<Character> perm = new ArrayList<>();
            perm.add(chars.get(0));
            perms.add(perm);
        } else {
            Character c = chars.get(chars.size() - 1);
            chars.remove(chars.size() - 1);
            List<List<Character>> prevPerms = generate(chars);
            for (List<Character> perm : prevPerms) {
                for (int i = 0; i <= perm.size(); i++) {
                    List<Character> newPerm = new ArrayList<>(perm);
                    newPerm.add(i, c);
                    perms.add(newPerm);
                }
            }
        }
        return perms;
    }

}
