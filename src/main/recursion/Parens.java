package recursion;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Виктор on 08.11.2016.
 */
public class Parens {

    public static void main(String[] args) {
        System.out.println(Arrays.toString(generate(3).toArray()));
    }

    private static Set<String> generate(int pairs) {
        Set<String> parens = new HashSet<>();
        if (pairs == 1) {
            parens.add("()");
        } else {
            Set<String> subParens = generate(pairs - 1);
            for (String sub : subParens) {
                parens.add("()" + sub);
                parens.add(sub + "()");
                parens.add("(" + sub + ")");
            }
        }
        return parens;
    }

}
