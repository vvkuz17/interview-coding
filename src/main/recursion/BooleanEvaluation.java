package recursion;

/**
 * Created by Виктор on 09.11.2016.
 */
public class BooleanEvaluation {

    public static void main(String[] args) {
        String expr = "1^0|0|1";
        System.out.println(countEval("0&0&0&1^1|0", true));
    }

    private static int countEval(String expr, boolean res) {
        int count = 0;
        if (expr.length() == 1)
            switch (expr.charAt(0)) {
                case '0':
                    count = res ? 0 : 1;
                    break;
                case '1':
                    count = res ? 1 : 0;
                    break;
            }
        else {
            for (int i = 1; i < expr.length(); i = i + 2) {
                char op = expr.charAt(i);
                String rightExpr = expr.substring(i + 1);
                String leftExpr = expr.substring(0, i);
                int rightTrueCount = countEval(rightExpr, true);
                int leftTrueCount = countEval(leftExpr, true);
                int rightFalseCount = countEval(rightExpr, false);
                int leftFalseCount = countEval(leftExpr, false);
                switch (op) {
                    case '&':
                        if (res) {
                            count += rightTrueCount * leftTrueCount;
                        } else {
                            count += rightFalseCount * leftTrueCount;
                            count += rightTrueCount * leftFalseCount;
                            count += rightFalseCount * leftFalseCount;
                        }
                        break;
                    case '|':
                        if (res) {
                            count += rightFalseCount * leftTrueCount;
                            count += rightTrueCount * leftFalseCount;
                            count += rightTrueCount * leftTrueCount;
                        } else {
                            count += rightFalseCount * leftFalseCount;
                        }
                        break;
                    case '^':
                        if (res) {
                            count += rightFalseCount * leftTrueCount;
                            count += rightTrueCount * leftFalseCount;
                        } else {
                            count += rightFalseCount * leftFalseCount;
                            count += rightTrueCount * leftTrueCount;
                        }
                        break;
                }
            }
        }
        return count;
    }

}
