package sort.search;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Виктор on 02.11.2016.
 */
public class ExternalSorting {

    private final String input;
    private final String output;
    private final int memoryInBytes;
    private final List<File> chunks = new ArrayList<>();

    public ExternalSorting(String input, String output, int memoryInBytes) {
        this.input = input;
        this.output = output;
        this.memoryInBytes = memoryInBytes;
    }

    public void sort() {
        sortChunks();
        mergeChunks();
        deleteChunks();
    }

    private void deleteChunks() {
        for (File chunk : chunks) {
            chunk.delete();
        }
    }

    private void mergeChunks() {
        List<BufferedReader> readers = openChunks();
        List<String> strings = new ArrayList<>();
        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(output)), memoryInBytes / (chunks.size() + 1))) {
            List<StringToReader> stringToReaders = readStrings(strings, readers);
            while (!stringToReaders.isEmpty()) {
                StringToReader min = Collections.min(stringToReaders);
                stringToReaders.remove(min);
                writer.write(min.value + "\n");
                String line = min.reader.readLine();
                if (line == null) {
                    min.reader.close();
                } else {
                    stringToReaders.add(new StringToReader(line, min.reader));
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private List<StringToReader> readStrings(List<String> strings, List<BufferedReader> readers) {
        List<StringToReader> result = new ArrayList<>();
        for (BufferedReader reader : readers) {
            try {
                String line = reader.readLine();
                if (line == null) continue;
                result.add(new StringToReader(line, reader));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    private List<BufferedReader> openChunks() {
        List<BufferedReader> readers = new ArrayList<>();
        for (File chunk : chunks) {
            try {
                readers.add(new BufferedReader(new InputStreamReader(new FileInputStream(chunk)), memoryInBytes / (chunks.size() + 1)));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        return readers;
    }

    private void sortChunks() {
        try (BufferedReader reader = Files.newBufferedReader(Paths.get(input))) {
            List<String> chunk = new ArrayList();
            int readed;
            do {
                readed = readChunk(chunk, reader);
                Collections.sort(chunk);
                writeChunk(chunk);
                chunk.clear();
            } while (readed != -1);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private int readChunk(List<String> chunk, BufferedReader reader) throws IOException {
        int sizeInBytes = 0;
        while (sizeInBytes < memoryInBytes) {
            String s = reader.readLine();
            if (s == null) return -1;
            sizeInBytes += s.length() * 2;
            chunk.add(s);
        }
        return 0;
    }

    private void writeChunk(List<String> buffer) throws IOException {
        File file = new File("chunk_" + chunks.size() + ".tmp");
        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)))) {
            for (String s : buffer) {
                writer.write(s + "\n");
            }
        }
        chunks.add(file);
    }

    private static class StringToReader implements Comparable<StringToReader> {

        String value;
        BufferedReader reader;

        public StringToReader(String value, BufferedReader reader) {
            this.value = value;
            this.reader = reader;
        }

        @Override
        public int compareTo(StringToReader o) {
            return this.value.compareTo(o.value);
        }

        @Override
        public String toString() {
            return value;
        }
    }
}
