package sort.search;

import java.util.Arrays;

/**
 * Created by Виктор on 21.10.2016.
 */
public class Quick {

    public static void main(String[] args) {
        int[] a = new int[]{5, 3, 10, 8, 12, 7, 19, 1, 3};
        new Quick().sort(a, 0, a.length - 1);
        System.out.println(Arrays.toString(a));
    }

    public void sort(int[] a, int l, int r) {
        int i = l;
        int j = r;
        int p = (j + i) / 2;
        while (i <= j) {
            while (a[i] < a[p]) i++;
            while (a[p] < a[j]) j--;
            if (i <= j) {
                int temp = a[i];
                a[i] = a[j];
                a[j] = temp;
                i++;
                j--;
            }
        }

        if (j + 1 > l) sort(a, l, j + 1);
        if (i < r) sort(a, i, r);
    }

}
