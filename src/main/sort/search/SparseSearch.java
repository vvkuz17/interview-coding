package sort.search;

/**
 * Created by Виктор on 02.11.2016.
 */
public class SparseSearch {

    public static void main(String[] args) {
        String[] array = new String[]{
                "at", "", "", "", "", "ball", "", "", "car", "", "", "dad"
        };
        System.out.println(binarySearch(array, 0, array.length - 1, "dad"));
    }

    private static int binarySearch(String[] array, int l, int r, String x) {
        if (l > r) return -1;
        int mid = (l + r) / 2;
        if (array[mid].isEmpty()) {
            int index = binarySearch(array, l, mid - 1, x);
            if (index == -1)
                index = binarySearch(array, mid + 1, r, x);
            return index;
        } else if (x.compareTo(array[mid]) < 0) {
            return binarySearch(array, l, mid - 1, x);
        } else if (x.compareTo(array[mid]) > 0) {
            return binarySearch(array, mid + 1, r, x);
        } else return mid;
    }

}
