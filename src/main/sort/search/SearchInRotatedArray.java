package sort.search;

/**
 * Created by Виктор on 01.11.2016.
 */
public class SearchInRotatedArray {

    public static void main(String[] args) {
        int[] array = new int[]{
                15, 16, 19, 20, 25,
                1, 3, 4, 5, 7, 10, 14};
        System.out.println(searchInRotated(array, 15));
    }

    private static int searchInRotated(int[] array, int x) {
        int maxIndex = searchMax(array, 0, array.length - 1);
        if (array[0] < x)
            return binarySearch(array, 0, maxIndex, x);
        else if (x < array[0])
            return binarySearch(array, maxIndex + 1, array.length - 1, x);
        else return 0;
    }

    private static int searchMax(int[] array, int l, int r) {
        if (l > r) return -1;
        int mid = (l + r) / 2;
        if ((mid + 1) < array.length && array[mid] > array[mid + 1]) return mid;
        if (array[l] > array[r]) {
            return searchMax(array, l, mid - 1);
        } else if (array[l] < array[r]) {
            return searchMax(array, mid + 1, r);
        } else return l;
    }

    private static int binarySearch(int[] array, int l, int r, int x) {
        if (l > r) return -1;
        int mid = (l + r) / 2;
        if (array[mid] > x) {
            return binarySearch(array, l, mid - 1, x);
        } else if (array[mid] < x) {
            return binarySearch(array, mid + 1, r, x);
        } else return mid;
    }

}
