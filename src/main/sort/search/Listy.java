package sort.search;

/**
 * Created by Виктор on 02.11.2016.
 */
public class Listy {

    private final int[] array;

    public Listy(int[] array) {
        if (array == null)
            throw new IllegalArgumentException("Argument is null.");
        this.array = array;
    }

    public int elementAt(int i) {
        if (i < 0 || i >= array.length)
            return -1;
        return array[i];
    }

    public int indexOf(int x) {
        if (x < 0)
            throw new IllegalArgumentException("Argument must be not negative.");

        int i = 1;
        while (elementAt(i) < x && elementAt(i) != -1) {
            i *= 2;
        }

        return binarySearch(i / 2, i, x);
    }

    private int binarySearch(int l, int r, int x) {
        if (l > r) return -1;
        int mid = (l + r) / 2;
        if (x < elementAt(mid) || elementAt(mid) == -1) {
            return binarySearch(l, mid - 1, x);
        } else if (elementAt(mid) < x) {
            return binarySearch(mid + 1, r, x);
        } else return mid;
    }

    public static void main(String[] args) {
        int[] array = new int[]{1, 3, 4, 5, 7, 10, 14, 15, 16, 19, 20, 25};
        Listy listy = new Listy(array);
        System.out.println(listy.indexOf(25));
    }
}
