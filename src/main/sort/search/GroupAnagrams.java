package sort.search;

import java.util.*;

/**
 * Created by Виктор on 01.11.2016.
 */
public class GroupAnagrams {

    static class Element {

        private final String value;
        private final int[] chars = new int['z']; // may be reduced
        private final int hashCode;

        public Element(String value) {
            this.value = value;
            count(value.toCharArray());
            hashCode = Arrays.hashCode(chars);
        }

        private void count(char[] charArray) {
            for (char c : charArray) {
                chars[c] = chars[c] + c;
            }
        }

        @Override
        public String toString() {
            return value;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Element element = (Element) o;

            return Arrays.equals(chars, element.chars);

        }

        @Override
        public int hashCode() {
            return hashCode;
        }
    }

    public static void main(String[] args) {
        String[] strings = new String[]{
                "abc",
                "bcd",
                "bca",
                "dcb",
                "sca"};

        Element[] elements = new Element[strings.length];
        for (int i = 0; i < strings.length; i++) {
            elements[i] = new Element(strings[i]);
        }

        HashMap<Element, List<Element>> map = new HashMap<>();

        for (int i = 0; i < elements.length; i++) {
            Element element = elements[i];
            List<Element> list = map.get(element);
            if (list == null) {
                list = new ArrayList<>();
                map.put(element, list);
            }
            list.add(element);
        }

        for (List<Element> list : map.values()) {
            System.out.println(list.toString());
        }
    }

}
