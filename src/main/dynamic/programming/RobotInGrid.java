package dynamic.programming;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Виктор on 07.11.2016.
 */
public class RobotInGrid {

    enum Step {
        RIGHT, DOWN;
    }

    private final boolean[][] grid;
    private final int r, c;

    private final Step[][] steps;

    public RobotInGrid(boolean[][] grid) {
        if (grid == null) throw new NullPointerException();
        this.grid = grid;
        r = grid.length - 1;
        c = grid[0].length - 1;
        steps = new Step[grid.length][grid[0].length];
    }

    public static void main(String[] args) {
        RobotInGrid robotInGrid = new RobotInGrid(
                new boolean[][]{
                        {true, true, true},
                        {false, true, false},
                        {true, true, true}}
        );
//        System.out.println(robotInGrid.topDownSearch());
        System.out.println(robotInGrid.bottomUpSearch());
    }

    private List<Step> bottomUpSearch() {
        calculate();
        return getPath(0, 0);
    }

    public List<Step> getPath(int row, int col) {
        List<Step> path = new LinkedList<>();
        do {
            Step step = steps[row][col];
            if (step == null) return null;
            path.add(step);
            switch (step) {
                case DOWN:
                    row++;
                    break;
                case RIGHT:
                    col++;
                    break;
            }
        } while (row < grid.length - 1 || col < grid[0].length - 1);
        return path;
    }

    private void calculate() {
        for (int row = 0; row < grid.length; row++) {
            for (int col = 0; col < grid[0].length; col++) {
                if (row < grid.length - 1 && grid[row + 1][col])
                    steps[row][col] = Step.DOWN;
                else if (col < grid[0].length - 1 && grid[row][col + 1])
                    steps[row][col] = Step.RIGHT;
            }
        }
    }

    /**
     * Top-Down approach
     *
     * @return list of steps
     */
    private List<Step> topDownSearch() {
        List<Step> path = new LinkedList<>();
        if (topDownSearch(r, c, path))
            return path;
        else
            return null;
    }

    public boolean topDownSearch(int row, int col, List<Step> path) {
        if (row == 0 && col == 0) // FINISH
            return true;
        if (row > 0 && grid[row - 1][col] && topDownSearch(row - 1, col, path)) {
            path.add(Step.DOWN);
            return true;
        } else if (col > 0 && grid[row][col - 1] && topDownSearch(row, col - 1, path)) {
            path.add(Step.RIGHT);
            return true;
        }
        return false;
    }

}
