package dynamic.programming;

import java.util.*;

/**
 * Created by Виктор on 09.11.2016.
 */
public class StackOfBoxes {

    public static void main(String[] args) {
        List<Box> boxes = new ArrayList<>();
        // ... add boxes ...

//        System.out.println(getTallestStack(boxes));

        LinkedList<Box> tallestStack = new LinkedList<>();
        Box[] boxesArray = boxes.toArray(new Box[]{});
        Arrays.sort(boxesArray, new Comparator<Box>() {
            @Override
            public int compare(Box o1, Box o2) {
                return Integer.compare(o2.h, o1.h);
            }
        });

        int maxHeight = 0;
        for (int i = 0; i < boxesArray.length; i++) {
            Box box = boxesArray[i];
            int height = computeTallestStackRecursive(boxesArray, i);
            if (height > maxHeight) {
                maxHeight = height;
            }
        }

        System.out.println(maxHeight);
    }

    private static int computeTallestStackRecursive(Box[] boxes, int bottomIndex) {
        int maxHeight = 0;
        Box bottom = boxes[bottomIndex];
        for (int i = bottomIndex + 1; i < boxes.length; i++) {
            Box box = boxes[i];
            if (!bottom.canBeBottom(box)) continue;
            int height = computeTallestStackRecursive(boxes, i);
            if (height > maxHeight) {
                maxHeight = height;
            }
        }
        return maxHeight + bottom.h;
    }

    /**
     * Bottom-Up Dynamic Programming Decision.
     *
     * @param boxes
     * @return tallest stack
     */
    private static List<Box> getTallestStack(List<Box> boxes) {
        int tallestIndex = 0;
        int maxHeight = sumHeights(boxes);
        LinkedList<Box>[] stacks = new LinkedList[maxHeight];
        stacks[0] = new LinkedList<>();
        for (int i = 1; i <= maxHeight; i++) {
            int maxW = 0, maxD = 0;
            for (Box box : boxes) {
                if (i - box.h < 0) continue;
                LinkedList<Box> stack = stacks[i - box.h];
                Box topBox = stack.element();
                if (stack != null &&
                        !stack.contains(box) &&
                        topBox.canBeBottom(box) &&
                        (box.w > maxW & box.d > maxD)) {
                    stacks[i] = new LinkedList<>(stack);
                    stacks[i].push(box);
                    maxD = box.d;
                    maxW = box.w;
                }
            }
            if (stacks[i] != null) tallestIndex = i;
        }
        return stacks[tallestIndex];
    }

    private static int sumHeights(List<Box> boxes) {
        int sum = 0;
        for (Box box : boxes) {
            sum += box.h;
        }
        return sum;
    }

    static class Box {

        private final int w, h, d;

        public Box(int w, int h, int d) {
            this.w = w;
            this.h = h;
            this.d = d;
        }

        boolean canBeBottom(Box box) {
            if (this.w > box.w & this.d > box.d & this.h > box.h)
                return true;
            return false;
        }
    }

}
