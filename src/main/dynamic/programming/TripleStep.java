package dynamic.programming;

import java.util.Arrays;

/**
 * Created by Виктор on 07.11.2016.
 */
public class TripleStep {

    public static void main(String[] args) {
        System.out.println(dynamicCountWays(15) + " " + countWaysWithMemo(15));
    }

    public static long dynamicCountWays(int n) {
        if (n < 0)
            throw new IllegalArgumentException("Argument must be non negative integer.");
        switch (n) {
            case 0:
                return 0;
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
                return 4;
        }

        long variants = 0, predStep = 4, predPredStep = 2, predPredPredStep = 1;
        for (int i = 4; i <= n; i++) {
            variants = predStep + predPredStep + predPredPredStep;
            predPredPredStep = predPredStep;
            predPredStep = predStep;
            predStep = variants;
        }
        return variants;
    }

    public static int countWaysWithMemo(int n) {
        int[] memo = new int[n + 1];
        Arrays.fill(memo, -1);
        int ways = countWaysWithMemo(n, memo);
        return ways;
    }

    public static int countWaysWithMemo(int n, int[] memo) {
        if (n < 0) {
            return 0;
        } else if (n == 0) {
            return 1;
        } else if (memo[n] > -1) {
            return memo[n];
        } else {
            memo[n] = countWaysWithMemo(n - 1, memo) + countWaysWithMemo(n - 2, memo) +
                    countWaysWithMemo(n - 3, memo);
            return memo[n];
        }
    }

}
