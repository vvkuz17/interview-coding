package string;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Виктор on 25.10.2016.
 */
public class ReverseString {

    public static void main(String[] args) {
        System.out.println(reverseStringByWord("quick red fox"));
    }

    private static String reverseStringByWord(String s) {
        char[] result = new char[s.length()];
        int j = s.length();
        for (int i = 0; i < s.length(); i++) {
            StringBuilder word = new StringBuilder();

            // read word
            while (i < s.length() && s.charAt(i) != ' ') {
                word.append(s.charAt(i));
                i++;
            }

            //write word
            j = j - word.length();
            for (int k = 0, m = j; k < word.length(); k++, m++) {
                result[m] = word.charAt(k);
            }

            if (i < s.length()) result[--j] = ' ';
        }

        return new String(result);
    }

}
