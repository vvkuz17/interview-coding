package bit.manipulation;

import java.util.Arrays;

/**
 * You have an integer and you can flip exactly one bit from a O to a 1. Write code to
 * topDownSearch the length of the longest sequence of 1s you could create.
 */
public class FlipBitToWin {

    public static void main(String[] args) {
        int number = -1;
//        System.out.println(Arrays.toString(new FlipBitToWin().collectOnes(number)));
        System.out.println(new FlipBitToWin().run(number));
    }

    public int run(int number) {
        if (number == -1) return Integer.SIZE;
        int[] arr = collectOnes(number);
        return getLongestOnesSequence(arr);
    }

    private int getLongestOnesSequence(int[] arr) {
        int max = 0, before = 0;
        boolean invert = false;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == 0) {
                if (!invert) invert = true;
                else {
                    max = Math.max(max, before + 1);
                    before = 0;
                }
            } else {
                if (!invert) before = arr[i];
                else {
                    max = Math.max(max, before + arr[i] + 1);
                    before = arr[i];
                    invert = false;
                }
            }
        }
        return max;
    }

    private int[] collectOnes(int number) {
        int[] arr = new int[32];
        Arrays.fill(arr, 0);
        int j = 0;
        for (int i = 0; i < 32; i++) {
            if (getBit(number, i)) arr[j]++;
            else {
                j = j + 2;
            }
        }
        return arr;
    }

    boolean getBit(int num, int i) {
        return ((num & (1 << i)) != 0);
    }

}
