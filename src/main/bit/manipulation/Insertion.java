package bit.manipulation;

import java.util.BitSet;

/**
 * Insertion: You are given two 32-bit numbers, N and M, and two bit positions, i and j. Write a method
 * to insert M into N such that M starts at bit j and ends at bit i. You can assume that the bits j through
 * i have enough space to fit all of M. That is, if M = 10011, you can assume that there are at least 5
 * bits between j and i. You would not, for example, have j = 3 and i = 2, because M could not fully
 * fit between bit 3 and bit 2.
 */
public class Insertion {

    public static void main(String[] args) {
        int n = -1;
        int m = 0;
        System.out.println(Integer.toBinaryString(new Insertion().insertion(n, m, 2, 6)));
    }

    public int insertion(int n, int m, int i, int j) {
        int mask = (-1 << j) | ((1 << (i - 1)) - 1);
        n = n & mask;
        return n | (m << i);
    }

}
