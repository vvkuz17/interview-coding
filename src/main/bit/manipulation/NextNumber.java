package bit.manipulation;

/**
 * Created by Виктор on 20.10.2016.
 */
public class NextNumber {

    public static void main(String[] args) {
        new NextNumber().run(1775);
    }

    public void run(int number) {
        int onesCount = countOnes(number);
        int smallest = (1 << onesCount) - 1;
        int largest = 0 - (1 << (Integer.SIZE - onesCount));

        System.out.println(getNext(number) + " " + Integer.toBinaryString(getNext(number)));
        System.out.println(getPrev(number) + " " + Integer.toBinaryString(getPrev(number)));
    }

    private int countOnes(int number) {
        int onesCount = 0;
        for (int i = 0; i < Integer.SIZE; i++) {
            if (((number >> i) & 1) == 1) onesCount++;
        }
        return onesCount;
    }

    public int getNext(int n) {
        int c = n;
        int c0 = 0;
        int c1 = 0;

        while ((c & 1) == 0 && c != 0) {
            c0++;
            c >>= 1;
        }

        while ((c & 1) == 1) {
            c1++;
            c >>= 1;
        }

        int p = c0 + c1;

        if (p == 31 || p == 0) {
            return -1;
        }

        n |= 1 << p;
        n &= ~((1 << p) - 1);
        n |= (1 << (c1 - 1)) - 1;
        return n;
    }

    public int getPrev(int n) {
        int c = n;
        int c0 = 0;
        int c1 = 0;

        while ((c & 1) == 1 && c != -1) {
            c1++;
            c >>= 1;
        }

        while ((c & 1) == 0) {
            c0++;
            c >>= 1;
        }

        int p = c0 + c1;

        if (p == 31 || p == -1) {
            return -1;
        }

        n &= -1 << (p + 1);
        n |= ((1 << (c1 + 1)) - 1) << (c0 - 1);
        return n;
    }

}
