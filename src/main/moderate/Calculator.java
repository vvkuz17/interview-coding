package moderate;

import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Виктор on 10.11.2016.
 */
public class Calculator {

    public static void main(String[] args) {
        System.out.println(new Calculator().eval("2*3+5/6*3+15"));
    }

    public double eval(String expr) {
        if (expr == null || expr.isEmpty())
            throw new IllegalArgumentException("Expression is null or empty.");
        List<Term> postfix = postfix(expr);
        return compute(postfix);
    }

    private double compute(List<Term> postfix) {
        Deque<Operand> stack = new LinkedList<>();
        for (Term term : postfix) {
            if (term instanceof Operation) {
                Operation op = (Operation) term;
                Operand v1 = stack.pop();
                Operand v2 = stack.pop();
                stack.push(new Operand(op.eval(v1, v2)));
            } else if (term instanceof Operand) {
                stack.push((Operand) term);
            }
        }
        return stack.element().getValue();
    }

    public List<Term> postfix(String expr) {
        List<Term> output = new LinkedList<>();
        Deque<Operation> stack = new LinkedList<>();
        Operand current = null;
        for (int i = 0; i < expr.length(); i++) {
            char c = expr.charAt(i);
            if (Operation.isOperation(c)) {
                output.add(current);
                current = null;

                Operation op = Operation.valueOf(c);
                while (!stack.isEmpty() && op.getPriority() <= stack.element().getPriority()) {
                    output.add(stack.pop());
                }
                stack.push(op);
            } else {
                if (current == null) current = new Operand();
                current.addSymbol(c);
            }
        }
        if (current != null) output.add(current);
        while (!stack.isEmpty()) {
            output.add(stack.pop());
        }
        return output;
    }

    private enum Operation implements Term {

        MULTIPLY('*'), DIVIDE('/'), PLUS('+'), MINUS('-'), UNDEFINED('?');

        char symbol;

        Operation(char symbol) {
            this.symbol = symbol;
        }

        static Operation valueOf(char symbol) {
            switch (symbol) {
                case '*':
                    return MULTIPLY;
                case '/':
                    return DIVIDE;
                case '+':
                    return PLUS;
                case '-':
                    return MINUS;
            }
            return UNDEFINED;
        }

        static boolean isOperation(char symbol) {
            return Operation.valueOf(symbol) != UNDEFINED;
        }

        double eval(Operand v1, Operand v2) {
            switch (this) {
                case PLUS:
                    return v1.getValue() + v2.getValue();
                case MINUS:
                    return v2.getValue() - v1.getValue();
                case MULTIPLY:
                    return v1.getValue() * v2.getValue();
                case DIVIDE:
                    return v2.getValue() / v1.getValue();
            }
            throw new IllegalStateException("Undefined operation.");
        }

        @Override
        public String toString() {
            return String.valueOf(symbol);
        }

        int getPriority() {
            return (this == MULTIPLY || this == DIVIDE) ? 1 : 0;
        }
    }

    private interface Term {
    }

    private class Operand implements Term {

        double value;
        StringBuilder sb;

        public Operand(double value) {
            this.value = value;
        }

        public Operand() {
        }

        boolean addSymbol(char symbol) {
            boolean status = true;
            try {
                Integer.parseInt(String.valueOf(symbol));
                if (sb == null) sb = new StringBuilder();
                sb.append(symbol);
            } catch (NumberFormatException ex) {
                status = false;
            }
            return status;
        }

        double getValue() {
            if (sb != null)
                return Double.parseDouble(sb.toString());
            else return value;
        }

        @Override
        public String toString() {
            return sb == null ? String.valueOf(value) : sb.toString();
        }
    }

}
