package moderate;

import java.util.HashMap;
import java.util.Map;

/**
 * Not thread-safe LRU cache.
 * <p>
 * Created by Виктор on 10.11.2016.
 */
public class LRUCache<K, V> {

    private final int capacity;
    private final Map<K, Element<K, V>> map;
    private Element<K, V> head; // tail is head.prev, tail.next is always null

    public LRUCache(int capacity) {
        this.capacity = capacity;
        this.map = new HashMap<>(capacity);
    }

    public void put(K key, V value) {
        Element element;
        if (map.containsKey(key)) {
            element = map.get(key);
            element.value = value;
        } else {
            element = new Element(key, value);
            if (map.size() == capacity) {
                remove(head.prev.key); // remove tail, because last-recently used
            }
            map.put(key, element);
        }
        moveToHead(element);
    }

    public V get(K key) {
        Element<K, V> element = map.get(key);
        if (element == null) return null;
        moveToHead(element);
        return element.value;
    }

    public V remove(K key) {
        if (!map.containsKey(key)) return null;
        Element<K, V> element = map.get(key);
        remove(element);
        map.remove(key, element);
        return element.value;
    }

    private void moveToHead(Element<K, V> element) {
        if (head == null) {
            head = element;
            head.prev = head;
            return;
        }
        if (element == head) return;
        remove(element);
        element.next = head;
        element.prev = head.prev;
        head.prev = element;
        head = element;
    }

    private void remove(Element<K, V> element) {
        if (element == head) {
            head.next.prev = head.prev;
            head = head.next;
        } else if (element == head.prev) // element is tail
        {
            head.prev = element.prev;
            element.prev.next = null;
        } else if (element.prev != null && element.next != null) {
            element.prev.next = element.next;
            element.next.prev = element.prev;
        }
    }

    private static class Element<K, V> {
        K key;
        V value;
        Element<K, V> next;
        Element<K, V> prev;

        public Element(K key, V value) {
            this.value = value;
            this.key = key;
        }
    }


}
