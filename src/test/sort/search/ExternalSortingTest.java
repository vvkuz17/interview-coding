package sort.search;

import org.junit.Test;

import java.io.*;
import java.nio.file.Paths;
import java.util.Random;

import static org.junit.Assert.*;

/**
 * Created by Виктор on 02.11.2016.
 */
public class ExternalSortingTest {

    @Test
    public void sort() throws Exception {
        int memory = 1000;
        String output = "output.txt";
        String input = "input.txt";

        Random random = new Random();
        File file = Paths.get(input).toFile();
        file.createNewFile();
        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)))) {
            int size = 0;
            while (size < memory * 10) {
                String str = String.valueOf(random.nextInt(100)) + "\n";
                writer.write(str);
                size += str.length() * 2;
            }
        }

        ExternalSorting externalSorting = new ExternalSorting(input, output, memory);
        externalSorting.sort();
    }

}