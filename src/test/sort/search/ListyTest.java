package sort.search;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Виктор on 02.11.2016.
 */
public class ListyTest {
    @Test
    public void indexOf() throws Exception {
        int[] array = new int[]{1, 3, 4, 5, 7, 10, 14, 15, 16, 19, 20, 25};
        Listy listy = new Listy(array);
        assertEquals(11, listy.indexOf(25));
        assertEquals(0, listy.indexOf(1));
        assertEquals(5, listy.indexOf(10));
    }

}