package moderate;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Виктор on 10.11.2016.
 */
public class LRUCacheTest {

    @Test
    public void putGetRemove() throws Exception {
        LRUCache<String, Integer> cache = new LRUCache<>(3);
        cache.put("one", 1);
        cache.put("two", 2);
        cache.put("three", 3);
        cache.put("four", 4);

        Assert.assertTrue(cache.get("one") == null);

        Assert.assertEquals(Integer.valueOf(2), cache.get("two"));

        cache.put("five", 5);
        Assert.assertTrue(cache.get("three") == null);

        cache.remove("four");
        Assert.assertTrue(cache.get("four") == null);


    }

}