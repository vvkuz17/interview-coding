package recursion;

import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;

/**
 * Created by Виктор on 08.11.2016.
 */
public class PowerSetTest {

    @Test
    public void allSubsets() throws Exception {

    }

    @Test
    public void isBit() throws Exception {
        Assert.assertEquals(true, new PowerSet(Collections.emptySet()).isBit(1, 0));
        Assert.assertEquals(false, new PowerSet(Collections.emptySet()).isBit(1, 1));
    }

    @Test
    public void pow2() throws Exception {
        Assert.assertEquals(4, new PowerSet(Collections.emptySet()).pow2(2));
        Assert.assertEquals(1, new PowerSet(Collections.emptySet()).pow2(0));
        Assert.assertEquals(2, new PowerSet(Collections.emptySet()).pow2(1));
    }

}